using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogues/Quest Dialogue")]

public class QuestDialogue : DialogueBase
{
    public QuestBase quest;
}
